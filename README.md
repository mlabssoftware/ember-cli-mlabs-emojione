# ember-cli-mlabs-emojione

A simple shim for [EmojiOne](https://github.com/Ranks/emojione) that exposes the library as an ES6 module.

This is a fork from the original addon available on https://github.com/crhayes/ember-cli-emojione

Installation
------------------------------------------------------------------------------

Please install this addon into your Ember.js project using ember-cli:
```
ember install ember-cli-mlabs-emojione
```

You can then import EmojiOne as an ES6 module:
````js
import emojione from 'emojione';
````
## Contributing

* `git clone <repository-url>` this repository
* `cd ember-cli-mlabs-emojione`
* `npm install`

### Linting

* `npm run lint:js`
* `npm run lint:js -- --fix`

### Running tests

* `ember test` – Runs the test suite on the current Ember version
* `ember test --server` – Runs the test suite in "watch mode"
* `ember try:each` – Runs the test suite against multiple Ember versions

### Running the dummy application

* `ember serve`
* Visit the dummy application at [http://localhost:4200](http://localhost:4200).

For more information on using ember-cli, visit [https://ember-cli.com/](https://ember-cli.com/).

License
------------------------------------------------------------------------------

This project is licensed under the [MIT License](LICENSE.md).
