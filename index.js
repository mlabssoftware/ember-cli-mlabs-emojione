'use strict';

module.exports = {
  name: 'ember-cli-mlabs-emojione',

  included: function(app) {
    this._super.included(app);
    
    app.import(require.resolve('emojione'));
    app.import('vendor/emojione.js', {
      exports: {
        emojione: ['default']
      }
    });
  }
};
